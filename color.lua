--
-- **color**
-- version : 1.1
--
-- auteur : _winterskill_ < [http://blenden-mark.alwaysdata.net](http://blenden-mark.alwaysdata.net) >
--
-- color est une petite librairie Lua qui enregistre des couleurs.
-- ainsi, dans les programmes, il suffit par exemple de faire ceci :
--
-- `require "color"`
-- `macouleur = color.red`
--
-- et `macouleur` vaudra `{255, 0, 0}`.
--
-- Cette librairie a surtout été pensée pour des utilisations dans le cadre du framework
-- [Löve2d](love2d.org).
--
-- color comporte un jeu de couleurs basique (`color`), un jeu de couleurs HTML (les couleurs
-- standard HTML) (`colorhtml`), un jeu de couleurs SVG (`colorsvg`) et un jeu très complet des
-- couleurs CSS (`colorcss`).
--

--[[
liste des couleurs :

black      :  noir
white      :  blanc
red        :  rouge
green      :  vert
blue       :  bleu
darkgrey   :  gris foncé
lightgrey  :  gris clair
darkred    :  rouge foncé
lightred   :  rouge clair
orange     :  orange
yellow     :  jaune
darkgreen  :  vert foncé
lightgreen :  vert clair
cyan       :  cyan
darkblue   :  bleu foncé
lightblue  :  bleu clair
violet     :  violet
purple     :  violet clair
]]

local Colors = {}

Colors.color = {
    -- les valeurs
    black = {000,000,000},
    white = {255,255,255},
    
    -- les couleurs principales
    red =   {255,000,000},
    green = {000,255,000},
    blue =  {000,000,255},
    
    -- les couleurs secondaires
    darkgrey =   {127,127,127},
    lightgrey =  {195,195,195},
    darkred =    {136,000,021},
    lightred =   {237,028,036},
    orange =     {255,127,000},
    yellow =     {255,240,000},
    darkgreen =  {034,177,076},
    lightgreen = {181,230,029},
    cyan =       {000,162,232},
    darkblue =   {062,072,204},
    lightblue =  {153,217,234},
    violet =     {163,072,164},
    purple =     {200,191,231}
}

-- les couleurs HTML
Colors.colorhtml = {
    aqua =    {000,255,255},
    black =   {000,000,000},
    blue =    {000,000,255},
    fuchsia = {255,000,255},
    green =   {000,128,000},
    gray =    {128,128,128},
    lime =    {000,255,000},
    maroon =  {128,000,000},
    navy =    {000,000,128},
    olive =   {128,128,000},
    purple =  {128,000,128},
    red =     {255,000,000},
    silver =  {192,192,192},
    teal =    {000,128,128},
    white =   {255,255,255},
    yellow =  {255,255,000}
}

-- les couleurs SVG
Colors.colorsvg = {
    -- couleurs rouges
    IndianRed =       {205,092,092},
    LightCoral =      {240,128,128},
    Salmon =          {250,128,114},
    DarkSalmon =      {233,150,122},
    LightSalmon =     {255,160,122},
    Crimson =         {220,020,060},
    Red =             {255,000,000},
    FireBrick =       {178,034,034},
    DarkRed =         {139,000,000},
    Maroon =          {128,000,000},
    Pink =            {255,192,203},
    LightPink =       {255,182,193},
    HotPink =         {255,105,180},
    DeepPink =        {255,020,147},
    MediumVioletRed = {199,021,133},
    PaleVioletRed =   {219,112,147},
    
    -- couleurs oranges
    Coral =      {255,127,080},
    Tomato =     {255,099,071},
    OrangeRed =  {255,069,000},
    DarkOrange = {255,140,000},
    Orange =     {255,165,000},
    
    -- couleurs jaunes
    Gold =                 {255,215,000},
    Yellow =               {255,255,000},
    LightYellow =          {255,255,224},
    LemonChiffon =         {255,250,205},
    LightGoldenrodYellow = {250,250,210},
    PapayaWhip =           {255,239,213},
    Moccasin =             {255,228,181},
    PeachPuff =            {255,218,185},
    PaleGoldenrod =        {238,232,170},
    Khaki =                {240,230,140},
    DarkKhaki =            {189,183,107},
    
    -- couleurs violettes
    Lavender =      {230,230,250},
    Thistle =       {216,191,216},
    Plum =          {221,160,221},
    Violet =        {238,130,238},
    Orchid =        {218,112,214},
    Fuchsia =       {255,000,255},
    Magenta =       {255,000,255},
    MadiumOrchid =  {186,085,211},
    MediumPurple =  {147,112,219},
    BlueViolet =    {138,043,226},
    DarkViolet =    {148,000,211},
    DarkOrchid =    {153,050,204},
    DarkMagenta =   {138,000,139},
    Purple =        {128,000,128},
    Indigo =        {075,000,130},
    SlateBlue =     {106,090,205},
    DarkSlateBlue = {072,061,139},
    
    -- couleurs vertes
    GreenYellow =       {173,255,047},
    Chartreuse =        {127,255,000},
    LawnGreen =         {124,252,000},
    Lime =              {000,255,000},
    LimeGreen =         {050,205,050},
    PaleGreen =         {152,251,152},
    LightGreen =        {144,238,144},
    MediumSpringGreen = {000,250,154},
    SpringGreen =       {000,255,127},
    MediumSeaGreen =    {060,179,113},
    SeaGreen =          {046,139,087},
    ForestGreen =       {034,139,034},
    Green =             {000,128,000},
    DarkGreen =         {000,100,000},
    YellowGreen =       {154,205,050},
    OliveDrab =         {107,142,035},
    Olive =             {128,128,000},
    DarkOliveGreen =    {085,107,047},
    MediumAquamarine =  {102,205,170},
    DarkSeaGreen =      {143,188,143},
    LightSeaGreen =     {032,178,170},
    DarkCyan =          {000,139,139},
    Teal =              {000,128,128},
    
    -- couleurs bleues
    Aqua =            {000,255,255},
    Cyan =            {000,255,255},
    LightCyan =       {224,255,255},
    PaleTurquoise =   {175,238,238},
    Aquamarine =      {127,255,212},
    Aquamaurine =     {127,255,212}, -- private joke
    Turquoise =       {064,224,208},
    MediumTurquoise = {072,209,204},
    DarkTurquoise =   {000,206,209},
    CadetBlue =       {095,158,160},
    SteelBlue =       {070,130,180},
    LightSteelBlue =  {176,196,222},
    PowderBlue =      {176,224,230},
    LightBlue =       {173,216,230},
    SkyBlue =         {135,206,235},
    LightSkyBlue =    {135,206,250},
    DeepSkyBlue =     {000,191,255},
    DodgerBlue =      {030,144,255},
    CornflowerBlue =  {100,149,237},
    MediumSlateBlue = {123,104,238},
    RoyalBlue =       {065,105,225},
    Blue =            {000,000,255},
    MediumBlue =      {000,000,205},
    DarkBlue =        {000,000,139},
    Navy =            {000,000,128},
    MidnightBlue =    {025,025,112},
    
    -- couleurs marron
    Cornsilk =       {255,248,220},
    BlanchedAlmond = {255,235,205},
    Bisque =         {255,228,196},
    NavajoWhite =    {255,222,179},
    Wheat =          {245,222,179},
    BurlyWood =      {222,184,135},
    Tan =            {210,180,140},
    RosyBrown =      {188,143,143},
    SandyBrown =     {244,164,096},
    Goldenrod =      {218,165,032},
    DarkGoldenrod =  {184,134,011},
    Peru =           {205,133,063},
    Chocolate =      {210,105,030},
    SaddleBrown =    {139,069,019},
    Sienna =         {160,082,045},
    Brown =          {165,042,042},
    
    -- couleurs blanches
    White =         {255,255,255},
    Snow =          {255,250,250},
    Honeydew =      {240,255,240},
    MintCream =     {245,255,250},
    Azure =         {240,255,255},
    AliceBlue =     {240,248,255},
    GhostWhite =    {248,248,255},
    WhiteSmoke =    {245,245,245},
    Seashell =      {255,245,238},
    Beige =         {245,245,220},
    OldLace =       {253,245,230},
    FloralWhite =   {255,250,240},
    Ivory =         {255,255,240},
    AntiqueWhite =  {250,235,215},
    Linen =         {250,240,230},
    LavenderBlush = {255,240,245},
    MistyRose =     {255,228,225},
    
    -- couleurs grises
    Gainsboro =      {220,220,220},
    LightGrey =      {211,211,211},
    Silver =         {192,192,192},
    DarkGray =       {169,169,169},
    Gray =           {128,128,128},
    DimGray =        {105,105,105},
    LightSlateGray = {119,136,153},
    SlateGray =      {112,128,144},
    DarkSlateGray =  {047,079,079},
    Black =          {000,000,000}
}

Colors.colorcss = {
    black =                {000,000,000},
    dimgray =              {105,105,105},
    dimgrey =              {105,105,105},
    gray =                 {128,128,128},
    grey =                 {128,128,128},
    darkgray =             {169,169,169},
    darkgrey =             {169,169,169},
    silver =               {192,192,192},
    lightgray =            {211,211,211},
    lightgrey =            {211,211,211},
    gainsboro =            {220,220,220},
    whitesmoke =           {245,245,245},
    white =                {255,255,255},
    maroon =               {128,000,000},
    darkred =              {139,000,000},
    red =                  {255,000,000},
    firebrick =            {178,034,034},
    brown =                {165,042,042},
    indianred =            {205,092,092},
    lightcoral =           {240,128,128},
    rosybrown =            {188,143,143},
    snow =                 {255,250,250},
    mistyrose =            {255,228,225},
    salmon =               {250,128,114},
    tomato =               {255,099,071},
    darksalmon =           {233,150,122},
    coral =                {255,127,080},
    orangered =            {255,069,000},
    lightsalmon =          {255,160,122},
    sienna =               {160,082,045},
    seashell =             {255,245,238},
    saddlebrown =          {139,069,019},
    chocolate =            {210,105,030},
    sandybrown =           {244,164,096},
    peachpuff =            {255,218,185},
    peru =                 {205,133,063},
    linen =                {250,240,230},
    bisque =               {255,228,196},
    darkorange =           {255,140,000},
    burlywood =            {222,184,135},
    tan =                  {210,180,140},
    antiquewhite =         {250,235,215},
    navajowhite =          {255,222,173},
    blanchedalmond =       {255,235,205},
    papayawhip =           {255,239,181},
    moccasin =             {255,228,181},
    orange =               {255,165,000},
    wheat =                {245,222,179},
    oldlace =              {253,245,230},
    floralwhite =          {255,250,240},
    darkgoldenrod =        {184,134,011},
    goldenrod =            {218,165,032},
    cornsilk =             {255,248,220},
    gold =                 {255,215,000},
    khaki =                {240,230,140},
    lemonchiffon =         {255,250,205},
    palegoldenrod =        {238,232,170},
    darkkhaki =            {189,183,107},
    olive =                {128,128,000},
    yellow =               {255,255,000},
    lightgoldenrodyellow = {250,250,210},
    lightyellow =          {255,255,224},
    beige =                {245,245,220},
    ivory =                {255,255,240},
    olivedrab =            {107,142,035},
    yellowgreen =          {154,205,050},
    darkolivegreen =       {085,107,047},
    greenyellow =          {173,255,047},
    chartreuse =           {127,255,000},
    lawngreen =            {124,252,000},
    darkgreen =            {000,100,000},
    green =                {000,128,000},
    lime =                 {000,255,000},
    limegreen =            {050,205,050},
    forestgreen =          {034,139,034},
    lightgreen =           {144,238,144},
    palegreen =            {152,251,152},
    darkseagreen =         {143,188,143},
    honeydew =             {240,255,240},
    seagreen =             {046,139,087},
    mediumseagreen =       {060,179,113},
    springgreen =          {000,255,127},
    mintcream =            {245,255,250},
    mediumspringgreen =    {000,205,170},
    mediumaquamarine =     {102,205,170},
    mediumaquamaurine =    {102,205,170}, -- private joke
    aquamarine =           {127,255,212},
    aquamaurine =          {127,255,212}, -- private joke
    turquoise =            {064,224,208},
    lightseagreen =        {032,178,170},
    mediumturquoise =      {072,209,204},
    teal =                 {000,128,128},
    darkcyan =             {000,139,139},
    aqua =                 {000,255,255},
    cyan =                 {000,255,255},
    darkslategrey =        {047,079,079},
    darkslategray =        {047,079,079},
    paleturquoise =        {175,238,238},
    lightcyan =            {224,225,255},
    azure =                {240,255,255},
    darkturquoise =        {000,206,209},
    cadetblue =            {095,158,160},
    powderblue =           {176,224,230},
    lightblue =            {173,216,230},
    deepskyblue =          {000,191,255},
    skyblue =              {135,206,235},
    lightskyblue =         {135,206,250},
    steelblue =            {070,130,180},
    aliceblue =            {240,248,255},
    dodgerblue =           {030,144,255},
    slategray =            {112,128,144},
    slategrey =            {112,128,144},
    lightslategray =       {119,136,153},
    lightslategrey =       {119,136,153},
    lightsteelblue =       {176,196,222},
    cornflowerblue =       {100,149,237},
    royalblue =            {065,105,225},
    navy =                 {000,000,128},
    darkblue =             {000,000,139},
    mediumblue =           {000,000,205},
    blue =                 {000,000,255},
    midnightblue =         {025,025,112},
    lavender =             {230,230,250},
    ghostwhite =           {248,248,255},
    slateblue =            {106,090,205},
    mediumslateblue =      {123,104,238},
    darkslateblue =        {072,061,139},
    mediumpurple =         {147,112,219},
    blueviolet =           {138,043,226},
    indigo =               {075,000,130},
    darkorchid =           {153,050,204},
    darkviolet =           {148,000,211},
    mediumorchid =         {186,085,211},
    purple =               {128,000,128},
    darkmagenta =          {139,000,139},
    fuchsia =              {255,000,255},
    magenta =              {255,000,255},
    violet =               {238,130,238},
    plum =                 {221,160,221},
    thistle =              {216,191,216},
    orchid =               {218,112,214},
    mediumvioletred =      {199,021,133},
    deeppink =             {255,020,147},
    hotpink =              {255,105,180},
    lavenderblush =        {255,240,245},
    palevioletred =        {219,112,147},
    crimson =              {220,020,060},
    pink =                 {255,192,203},
    lightpink =            {255,182,193}
}

return Colors
